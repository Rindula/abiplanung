<?php
namespace App\Controller;

/**
 * Finances Controller
 *
 * @property \App\Model\Table\FinancesTable $Finances
 *
 * @method \App\Model\Entity\Finance[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FinancesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $finances = $this->paginate($this->Finances);

        $balance = $this->Finances->find("all")
            ->where([
                "deleted" => 0
            ])
            ->sumOf("betrag");

        $this->set(compact('finances', 'balance'));
    }

    /**
     * View method
     *
     * @param string|null $id Finance id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finance = $this->Finances->get($id, [
            'contain' => []
        ]);

        $this->set('finance', $finance);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finance = $this->Finances->newEntity();
        if ($this->request->is('post')) {
            $finance = $this->Finances->patchEntity($finance, $this->request->getData());
            if ($this->Finances->save($finance)) {
                $this->Flash->success(__('The finance has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The finance could not be saved. Please, try again.'));
        }
        $this->set(compact('finance'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Finance id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finance = $this->Finances->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $finance = $this->Finances->patchEntity($finance, $this->request->getData());
            if ($this->Finances->save($finance)) {
                $this->Flash->success(__('The finance has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The finance could not be saved. Please, try again.'));
        }
        $this->set(compact('finance'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Finance id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $finance = $this->Finances->get($id);
        $finance->deleted = true;
        if ($this->Finances->save($finance)) {
            $this->Flash->success(__('The finance has been deleted.'));
        } else {
            $this->Flash->error(__('The finance could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Recreate method
     *
     * @param string|null $id Finance id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function recreate($id = null)
    {
        $this->request->allowMethod(['post']);
        $finance = $this->Finances->get($id);
        $finance->deleted = false;
        if ($this->Finances->save($finance)) {
            $this->Flash->success(__('The finance has been recreated.'));
        } else {
            $this->Flash->error(__('The finance could not be recreated. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
