<?php
namespace App\Controller;

use Cake\Http\Exception\MethodNotAllowedException;

/**
 * Documents Controller
 */
class DocumentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $documents = ["Anschreiben"];
        $this->set(compact('documents'));
    }

    public function anschreiben($method = null)
    {
        if (empty($method)) return $this->redirect(['controller' => 'documents', "action" => 'anschreiben', 'params']);
        $this->viewBuilder()->setTemplate("anschreiben/$method");

        switch ($method) {
            case 'params':
                return;

            case 'view':
                $this->viewBuilder()->setLayout('pdf');
                $anzeigegroessen = [];
                if ($_GET["a6_possible"] == "on") {
                    $anzeigegroessen[] = "A6 ({$_GET["a6_price"]}€)";
                }
                if ($_GET["a5_possible"] == "on") {
                    $anzeigegroessen[] = "A5 ({$_GET["a5_price"]}€)";
                }
                if ($_GET["a4_possible"] == "on") {
                    $anzeigegroessen[] = "A4 ({$_GET["a4_price"]}€)";
                }

                $anzeigegroessen = join(", ", $anzeigegroessen);

                $this->set(compact('anzeigegroessen'));
                break;
            default:
                throw new MethodNotAllowedException();
        }
    }

}
