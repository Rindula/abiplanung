<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Impressum Controller
 *
 *
 * @method \App\Model\Entity\Impressum[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ImpressumController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $impressum = $this->paginate($this->Impressum);

        $this->set(compact('impressum'));
    }

    /**
     * View method
     *
     * @param string|null $id Impressum id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $impressum = $this->Impressum->get($id, [
            'contain' => []
        ]);

        $this->set('impressum', $impressum);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $impressum = $this->Impressum->newEntity();
        if ($this->request->is('post')) {
            $impressum = $this->Impressum->patchEntity($impressum, $this->request->getData());
            if ($this->Impressum->save($impressum)) {
                $this->Flash->success(__('The impressum has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The impressum could not be saved. Please, try again.'));
        }
        $this->set(compact('impressum'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Impressum id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $impressum = $this->Impressum->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $impressum = $this->Impressum->patchEntity($impressum, $this->request->getData());
            if ($this->Impressum->save($impressum)) {
                $this->Flash->success(__('The impressum has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The impressum could not be saved. Please, try again.'));
        }
        $this->set(compact('impressum'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Impressum id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $impressum = $this->Impressum->get($id);
        if ($this->Impressum->delete($impressum)) {
            $this->Flash->success(__('The impressum has been deleted.'));
        } else {
            $this->Flash->error(__('The impressum could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
