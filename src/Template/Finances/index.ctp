<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Finance[]|\Cake\Collection\CollectionInterface $finances
 */
?>
<div class="finances index large-9 medium-8 columns content">
    <h3><?= __('Finances') ?></h3>
    <h4>Aktueller Stand: <?= $this->Number->currency($balance, "EUR") ?></h4>
    <table class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('betrag') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($finances as $finance): ?>
                <tr class="<?= ($finance->deleted) ? "text-muted" : "" ?>">
                <td><?= $this->Number->currency($finance->betrag, "EUR") ?></td>
                <td><?= h($finance->created) ?></td>
                <td><?= h($finance->modified) ?></td>
                    <?php if (!$finance->deleted): ?>
                        <td class="btn-group">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $finance->id], ['class' => 'btn btn-outline-primary']) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $finance->id], ['class' => 'btn btn-outline-secondary']) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $finance->id], ['confirm' => __('Bist du sicher, dass du den Betrag #{0} entfernen möchtest?', $finance->id), 'class' => 'btn btn-outline-danger']) ?>
                </td>
                    <?php else: ?>
                        <td class="btn-group">
                            <?= $this->Form->postLink(__('Wiederherstellen'), ['action' => 'recreate', $finance->id], ['class' => 'btn btn-outline-dark']) ?>
                        </td>
                    <?php endif; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
