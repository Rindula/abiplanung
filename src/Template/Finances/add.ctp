<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Finance $finance
 */
?>
<div class="finances form large-9 medium-8 columns content">
    <?= $this->Form->create($finance) ?>
    <fieldset>
        <legend><?= __('Add Finance') ?></legend>
        <?php
            echo $this->Form->control('betrag');
            echo $this->Form->control('note');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
