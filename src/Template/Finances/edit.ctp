<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Finance $finance
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $finance->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $finance->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Finances'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="finances form large-9 medium-8 columns content">
    <?= $this->Form->create($finance) ?>
    <fieldset>
        <legend><?= __('Edit Finance') ?></legend>
        <?php
            echo $this->Form->control('betrag');
            echo $this->Form->control('note');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
