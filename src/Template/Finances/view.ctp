<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Finance $finance
 */
?>
<div class="finances view large-9 medium-8 columns content">
    <h3>Übersicht des Zahlungsverfahrens</h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($finance->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Betrag') ?></th>
            <td><?= $this->Number->currency($finance->betrag, 'EUR') ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($finance->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($finance->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4 class="col-3"><?= __('Note') ?></h4>
        <div class="col-9"><?= $this->Text->autoParagraph(h($finance->note)); ?></div>
    </div>
</div>
