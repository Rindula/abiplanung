<?php

$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);

$pdf->SetCreator("rindula.de - Abiplanung");
$pdf->SetAuthor($_GET['name']);
$pdf->SetTitle("Firmenanschreiben");

$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->AddPage();
$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

$pdf->SetFontSize(11);

$html_head = "<address>Abiturkomitee der Friedrich-Hecker-Schule Sinsheim<br>
{$_GET["name"]}<br>
{$_GET["street_nr"]}<br>
{$_GET["city"]}<br>
{$_GET["email"]}</address>";
$pdf->writeHTMLCell(100, 0, $pdf->GetX(), $pdf->GetY(), $html_head, 0, 1, false, true, 'L');

$pdf->SetXY(-60, $pdf->GetY() + 20);
$html_date = "Sinsheim, " . date("F Y");
$pdf->writeHTMLCell(50, 0, $pdf->GetX(), $pdf->GetY(), $html_date, 0, 1, false, true, 'R');
$pdf->SetY($pdf->GetY() + 20);
$html_text = "<p><b>Sponsoring Abiturfeier {$_GET["end_date"]["year"]} Friedrich-Hecker-Schule</b></p><p>Sehr geehrte Damen und Herren,</p><p>zur Finanzierung der Feierlichkeiten anlässlich unserer Zeugnisübergabe sind wir, die Schüler der Jahrgangsstufe 2 der Friedrich-Hecker-Schule Sinsheim, auf der Suche nach Sponsoren.</p><p>Daher möchten wir ihnen anbiten in unserer Abitur-Zeitung für ihr Unternehmen Werbung zu machen.</p><p>Damit erreichen Sie über 180 Schüler des Technischen Gymnasiums und sprechen zusätzlich auch Schüler anderer Schularten, wie der Berufsfachschule, sowie Geschwister, Eltern und Lehrer der Schüler an.</p><p>Mögliche Anzeigegrößen in unserer Zeitung sind {$anzeigegroessen}.</p><p>Bei Interesse bitten wir Sie sich bei uns schnellst möglich, spätestens jedoch bis zum " . date("d.m.Y", strtotime($_GET["end_date"]["year"] . "-" . $_GET["end_date"]["month"] . "-" . $_GET["end_date"]["day"])) . " bei uns zu melden.</p><p>Gerne sind wir bereit Ihnen eine Rechnung und/oder eine Quittung auszustellen.</p><p>Mit Freundlichen Grüßen</p><p>{$_GET["name"]}<br>Mitglied im Abiturkomitee</p>";
$pdf->writeHTMLCell(0, 0, $pdf->GetX(), $pdf->GetY(), $html_text, 0, 1, false, true, 'L');

$pdf->Output('anschreiben.pdf', 'I');
