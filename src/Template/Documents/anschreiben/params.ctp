<h2>Parameter setzen</h2>
<?= $this->Form->create(null, ['url' => ['action' => 'anschreiben', 'view'], 'method' => 'get']); ?>
<h3>Generelle Werte</h3>
<?= $this->Form->control('end_date', ['type' => 'date', 'label' => 'Rückmeldedatum']) ?>
<h3>Werbemöglichkeiten</h3>
<?= $this->Form->control('a6_possible', ['type' => 'checkbox', 'label' => 'A6 möglich']) ?>
<?= $this->Form->control('a6_price', ['type' => 'number', 'label' => 'A6 Preis', 'default' => 70, 'step' => '0.01']) ?>
<?= $this->Form->control('a5_possible', ['type' => 'checkbox', 'label' => 'A5 möglich']) ?>
<?= $this->Form->control('a5_price', ['type' => 'number', 'label' => 'A5 Preis', 'default' => 120, 'step' => '0.01']) ?>
<?= $this->Form->control('a4_possible', ['type' => 'checkbox', 'label' => 'A4 möglich']) ?>
<?= $this->Form->control('a4_price', ['type' => 'number', 'label' => 'A4 Preis', 'default' => 200, 'step' => '0.01']) ?>
<h3>Kontaktdaten für Rückantwort</h3>
<?= $this->Form->control('name', ['type' => 'name']) ?>
<?= $this->Form->control('street_nr', ['type' => 'street']) ?>
<?= $this->Form->control('city', ['type' => 'city']) ?>
<?= $this->Form->control('email', ['type' => 'email']) ?>
<br>
<?= $this->Form->submit("Generieren") ?>
<?= $this->Form->end(); ?>
<br>
<br>
<br>
