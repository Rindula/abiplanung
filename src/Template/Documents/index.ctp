<?php foreach ($documents as $document): ?>

    <li><?= $this->Html->link($document, ["controller" => 'documents', 'action' => strtolower($document)]) ?></li>

<?php endforeach; ?>
