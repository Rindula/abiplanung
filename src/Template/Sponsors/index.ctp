<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sponsor[]|\Cake\Collection\CollectionInterface $sponsors
 */
?>
<?= $this->Html->link(__('New Sponsor'), ['action' => 'add'], ["class" => 'btn btn-primary']) ?>

<div class="sponsors index large-9 medium-8 columns content">
    <h3><?= __('Sponsors') ?></h3>
    <table class="table">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('belegexemplar') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sponsors as $sponsor): ?>
            <tr>
                <td><?= h($sponsor->name) ?></td>
                <td><?= h($sponsor->status->name) ?></td>
                <td><?= __(($sponsor->belegexemplar) ? __("Yes") : __("No")) ?></td>
                <td class="btn-group">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sponsor->id], ['class' => 'btn btn-outline-primary']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sponsor->id], ['class' => 'btn btn-outline-secondary']) ?>
                    <?= (false) ? $this->Form->postLink(__('Delete'), ['action' => 'delete', $sponsor->id], ['confirm' => __('Are you sure you want to delete {0}?', $sponsor->name),'class' => 'btn btn-outline-danger']) : "" ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <nav class="Page navigation">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </nav>
</div>
