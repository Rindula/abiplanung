<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sponsor $sponsor
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sponsor'), ['action' => 'edit', $sponsor->id]) ?> </li>
        <!-- <li><?= ""//$this->Form->postLink(__('Delete Sponsor'), ['action' => 'delete', $sponsor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sponsor->id)]) ?> </li> -->
        <li><?= $this->Html->link(__('List Sponsors'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sponsor'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sponsors view large-9 medium-8 columns content">
    <h3><?= h($sponsor->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($sponsor->name) ?></td>
        </tr>
        <?php if (isset($sponsor->email) && !empty($sponsor->email)): ?>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= $this->Html->link("E-Mail senden", ["controller" => 'mails', 'action' => 'send', h($sponsor->id)]) ?></td>
        </tr>
        <?php endif; ?>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($sponsor->status->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Belegexemplar') ?></th>
            <td><?= __(($sponsor->belegexemplar) ? __("Yes") : __("No")) ?></td>
        </tr>
    </table>
</div>
