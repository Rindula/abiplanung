<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sponsor $sponsor
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <!-- <li><?= /*$this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $sponsor->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $sponsor->id)]
            )*/""
        ?></li> -->
        <li><?= $this->Html->link(__('List Sponsors'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="sponsors form large-9 medium-8 columns content">
    <?= $this->Form->create($sponsor) ?>
    <fieldset>
        <legend><?= __('Edit Sponsor') ?></legend>
        <?php
            echo $this->Form->control('name', ["required" => true]);
            echo $this->Form->control('status_id', ["required" => true, "type" => "select", "options" => $statuses]);
            echo $this->Form->control('email', ["required" => false]);
            echo $this->Form->control('belegexemplar', ["required" => true, "type" => "select", "options" => array(1 => __("Yes"), 0 => __("No"))]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
