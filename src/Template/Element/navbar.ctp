<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top mb-4">
  <a class="navbar-brand" href="#">rindula.de</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item<?= ($this->request->getParam('controller') == "Pages") ? " active" : "" ?>">
          <?= $this->Html->link(__("Home"), ['controller' => '', 'action' => 'index'], ["class" => "nav-link"]) ?>
      </li>
      <li class="nav-item<?= ($this->request->getParam('controller') == "Sponsors") ? " active" : "" ?>">
          <?= $this->Html->link(__("Sponsors"), ['controller' => 'Sponsors', 'action' => 'index'], ["class"=>"nav-link"]) ?>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?= __("Financial planning") ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <?= $this->Html->link(__("Overview"), ["controller" => "Finances", "action" => "index"], ["class" => "dropdown-item"]) ?>
            <?= $this->Html->link("Geldfluss registrieren", ["controller" => "Finances", "action" => "add"], ["class" => "dropdown-item"]) ?>
        </div>
      </li>
        <li class="nav-item<?= ($this->request->getParam('controller') == "Documents") ? " active" : "" ?>">
            <?= $this->Html->link(__("Documents"), ['controller' => 'Documents', 'action' => 'index'], ["class" => "nav-link"]) ?>
        </li>
    </ul>
  </div>
    <?= getGit() ?>
</nav>
