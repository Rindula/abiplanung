<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinancesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinancesTable Test Case
 */
class FinancesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FinancesTable
     */
    public $Finances;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Finances'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Finances') ? [] : ['className' => FinancesTable::class];
        $this->Finances = TableRegistry::getTableLocator()->get('Finances', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Finances);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
