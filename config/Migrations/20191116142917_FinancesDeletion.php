<?php

use Migrations\AbstractMigration;

class FinancesDeletion extends AbstractMigration
{

    public function up()
    {

        $this->table('finances')
            ->addColumn('deleted', 'boolean', [
                'after' => 'modified',
                'default' => '0',
                'length' => null,
                'null' => false,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('finances')
            ->removeColumn('deleted')
            ->update();
    }
}

