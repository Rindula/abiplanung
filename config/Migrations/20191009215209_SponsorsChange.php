<?php
use Migrations\AbstractMigration;

class SponsorsChange extends AbstractMigration
{

    public $autoId = false;

    public function up()
    {

        $this->table('sponsors')
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('status_id', 'integer', [
                'default' => '1',
                'limit' => 11,
                'null' => false,
                'signed' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('belegexemplar', 'integer', [
                'default' => '0',
                'limit' => 1,
                'null' => false,
            ])
            ->addIndex(
                [
                    'status_id',
                ]
            )
            ->create();

        $this->table('status')
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('alias', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->create();

            $rows = [
                [
                  'name'  => 'Nicht angeschrieben',
                  'alias'  => 'not_contacted'
                ],
                [
                  'name'  => 'Warte auf Antwort',
                  'alias' => 'awaiting'
                ],
                [
                  'name'  => 'Zugesagt',
                  'alias' => 'applied'
                ],
                [
                  'name'  => 'Abgesagt',
                  'alias' => 'denied'
                ]
            ];
    
            $this->table('status')->insert($rows)->save();

        $this->table('sponsors')
            ->addForeignKey(
                'status_id',
                'status',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('sponsors')
            ->dropForeignKey(
                'status_id'
            )->save();

        $this->table('sponsors')->drop()->save();
        $this->table('status')->drop()->save();
    }
}
