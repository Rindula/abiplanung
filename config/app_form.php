<?php

return [
    'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>',
    'formGroup' => '<tr><td>{{label}}</td><td>{{input}}</td></tr>',
    'input' => '<input class="form-control" type="{{type}}" name="{{name}}"{{attrs}}/>',
    'selectMultiple' => '<select class="form-control" name="{{name}}[]" multiple="multiple"{{attrs}}>{{content}}</select>',
    'select' => '<select class="form-control" name="{{name}}"{{attrs}}>{{content}}</select>',
    'checkbox' => '<input type="checkbox" class="form-check-input" name="{{name}}"{{attrs}}/>',
    'button' => '<button class="btn btn-outline-secondary" {{attrs}}>{{text}}</button>',
    'textarea' => '<textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea>',
    'label' => '<label{{attrs}}>{{text}}</label>',
];
